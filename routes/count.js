const express = require ('express');
const router = express.Router();


router.get("/", (req,res) => {
    res.render('index.pug');
});

router.get("/sort", (req,res) => {
    res.render('index.pug');
});

router.post("/sort", (req,res) => {
    //console.log(req.body.array);
    
    try{
        let inputArr=   JSON.parse("[" + req.body.input + "]");
        let freqCount= new Map();
        inputArr.forEach( num => {
            let freq= freqCount.get(num);
            freqCount.set(num,(freq===undefined?1:freq+1))
        });
        let stringArray=[];
        let jsonArray=[];
        let ans= Array.from(freqCount.keys()).sort((a, b) => {
            if(freqCount.get(a)===freqCount.get(b)) {return a-b}
            else {return freqCount.get(b)-freqCount.get(a);}
        });
        ans.forEach(e=>{
            //console.log(e+"---->"+freqCount.get(e));
            stringArray.push(freqCount.get(e)<2?`${e} is ${freqCount.get(e)} time`:`${e} is ${freqCount.get(e)} times`);
            jsonArray.push([e,freqCount.get(e)]);
        });

        res.render('index',{stringArray,inputArr});
    }
    catch(error){
        res.render('index',{error:"Wrong input", inputArr:req.body.input});

    }
 
});


module.exports= router;
