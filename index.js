const express = require ('express');
const bodyParser = require ('body-parser');

const app = express();

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({extended : false}));

const routes= require('./routes/count');
app.use('/',routes);

app.use(( req, res, next)=>{
    const error = new Error('Not Found');
    error.status=404;
    console.dir(error);
    next(error);
});

app.use((err,req,res,next)=>{
    res.locals.error=err;
    res.status(err.status);
    //console.dir(res.locals.error);
    res.render('error');
});


app.listen(process.env.PORT||3000, ()=>{
    console.log(`started app on localhost:3000 at ${new Date()}`); 
});